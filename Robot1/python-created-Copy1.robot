# -*- coding: utf-8 -*-
*** Settings ***
Library     RPA.Desktop
Library     RPA.Windows
Library     RPA.Browser

***Tasks***


# +
***Tasks***

Sequenz
     Control Window     class:'Shell_TrayWnd'
    RPA.Windows.Click    name:'Running applications' > type:Button and name:'File Explorer'


File Explorer
    Control Window    name:'File Explorer'
    RPA.Windows.Click    name:'Tree View' > type:TreeItem and name:'Documents (pinned)'


Documents
    Control Window    name:'Documents'
    RPA.Windows.Double Click    name:'Items View' > automationid:5 > name:'Name'


TEST.txt - Notepad
    Control Window    name:'TEST.txt - Notepad'
    RPA.Desktop.Type text    Das\ \ist\ \ein\ \Test.    enter=true


-TEST.txt - Notepad
    Control Window    name:'*TEST.txt - Notepad'
    RPA.Desktop.Press keys    Ctrl  s


TEST.txt - Notepad
    Control Window    name:'TEST.txt - Notepad'
    RPA.Desktop.Press keys    Ctrl  a
    Control Window    name:'TEST.txt - Notepad'
    RPA.Desktop.Press keys    Ctrl  c
    Control Window    name:'TEST.txt - Notepad'
    Close Current Window


Sequenz
     Control Window     class:'Shell_TrayWnd'
    RPA.Windows.Click    name:'Running applications' > type:Button and name:'Chromium'


New Tab - Chromium
    Control Window    name:'New Tab - Chromium'
    RPA.Windows.Click    name:'Bookmarks' > name:'Drive'


Meine Ablage – Google Drive - Chromium
    RPA.Browser.Attach Chrome Browser    9222
    RPA.Browser.Switch Window    locator=Meine Ablage – Google Drive
    RPA.Browser.Double Click Element    xpath://*[@id=':4']//DIV[contains(text(),'MA_process')]


MA_process – Google Drive - Chromium
    Control Window    name:'MA_process – Google Drive - Chromium'
    RPA.Windows.Click    type:MenuItem and name:'Neu'
    RPA.Browser.Attach Chrome Browser    9222
    RPA.Browser.Switch Window    locator=MA_process – Google Drive
    RPA.Browser.Click Element    xpath://*[@id=':4o']//DIV[contains(text(),'Google Docs')]


Unbenanntes Dokument - Google Docs - Chromium
    RPA.Browser.Attach Chrome Browser    9222
    RPA.Browser.Switch Window    locator=Unbenanntes Dokument - Google Docs
    RPA.Browser.Click Element    xpath://INPUT
    Control Window    name:'Unbenanntes Dokument - Google Docs - Chromium'
    RPA.Desktop.Press keys    Ctrl  a
    Control Window    name:'Unbenanntes Dokument - Google Docs - Chromium'
    RPA.Desktop.Type text    hallo\ \    enter=true
    RPA.Browser.Attach Chrome Browser    9222
    RPA.Browser.Switch Window    locator=hallo - Google Docs
    RPA.Browser.Click Element    xpath://*[local-name() = 'svg'][not(*)]


hallo - Google Docs - Chromium
    Control Window    name:'hallo - Google Docs - Chromium'
    RPA.Desktop.Press keys    Ctrl  v
    Control Window    name:'hallo - Google Docs - Chromium'
    RPA.Windows.Click    
    Control Window    name:'hallo - Google Docs - Chromium'
    RPA.Desktop.Press keys    None  enter
    RPA.Browser.Attach Chrome Browser    9222
    RPA.Browser.Switch Window    locator=hallo - Google Docs
    RPA.Browser.Double Click Element    xpath://DIV
    RPA.Browser.Attach Chrome Browser    9222
    RPA.Browser.Switch Window    locator=hallo - Google Docs
    RPA.Browser.Click Element    xpath://DIV[contains(text(),'Normaler Text')]
    RPA.Browser.Attach Chrome Browser    9222
    RPA.Browser.Switch Window    locator=hallo - Google Docs
    RPA.Browser.Click Element    xpath://SPAN[contains(text(),'Überschrift 1')]
    Control Window    name:'hallo - Google Docs - Chromium'
    RPA.Windows.Click    name:'Menüleiste' > name:'Festnetz' > type:Button and name:'Fett (Strg+B)'


