class Sequence:
    def __init__(self, name, number):
        self.name = name
        self.number = number
        self.click_array = []
        self.type_into_array = []
        self.ui_input_array = []

    def __repr__(self):
        return "<Sequence name:%s number:%s ui_input_array:%s>" % (
            self.name, self.number, self.ui_input_array)
