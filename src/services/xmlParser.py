import xml.etree.ElementTree as ET
from src.models.sequence import Sequence
from src.models.click import Click
from src.models.sendHotKey import SendHotKey
import src.services.selectorService as selector_service

from src.models.typeInto import TypeInto

ns = {
    "": "http://schemas.microsoft.com/netfx/2009/xaml/activities",
    "av": "http://schemas.microsoft.com/winfx/2006/xaml/presentation",
    "mc": "http://schemas.openxmlformats.org/markup-compatibility/2006",
    "mva": "clr-namespace:Microsoft.VisualBasic.Activities;assembly=System.Activities",
    "sap": "http://schemas.microsoft.com/netfx/2009/xaml/activities/presentation",
    "sap2010": "http://schemas.microsoft.com/netfx/2010/xaml/activities/presentation",
    "scg": "clr-namespace:System.Collections.Generic;assembly=mscorlib",
    "sco": "clr-namespace:System.Collections.ObjectModel;assembly=mscorlib",
    "ui": "http://schemas.uipath.com/workflow/activities",
    "x": "http://schemas.microsoft.com/winfx/2006/xaml"
}


def parse_sequences(xml_file_name):
    tree = ET.parse(xml_file_name)
    root = tree.getroot()
    sequences = []

    for sequence_xml in root.findall('.//Sequence', ns):
        sequence: Sequence = create_sequence(sequence_xml)
        ui_inputs_for_sequence = []

        for click_xml in sequence_xml.findall('.//ui:Click', ns):
            create_click(sequence_xml, click_xml, ui_inputs_for_sequence)

        for hotkey_xml in sequence_xml.findall('.//ui:SendHotkey', ns):
            create_hotkey(sequence_xml, hotkey_xml, ui_inputs_for_sequence)

        for type_into_xml in sequence_xml.findall('.//ui:TypeInto', ns):
            create_type_into(sequence_xml, type_into_xml, ui_inputs_for_sequence)

        append_sorted_ui_inputs(ui_inputs_for_sequence, sequence)
        sequences.append(sequence)

    return sequences


def create_sequence(sequence_xml):
    name_ = sequence_xml.attrib['DisplayName']
    name_ = name_.replace("\n", " ")
    schema_sap2010 = ns.get("sap2010")
    sequence_no = sequence_xml.attrib['{' + schema_sap2010 + '}WorkflowViewState.IdRef']
    return Sequence(name_, sequence_no)


def create_click(sequence_xml, click_xml, ui_inputs_for_sequence):
    ui_input_index = list(sequence_xml).index(click_xml)
    click = Click(ui_input_index)
    add_click_attributes(click, click_xml)
    click_target = click_xml.find('.//ui:Click.Target/ui:Target', ns)
    selector_service.add_selectors(click_target, click)

    #  there should only be one target per click
    """
    for click_target in click_xml.findall('.//ui:Click.Target/ui:Target', ns):
        selector_service.add_selectors(click_target, click)
    """

    ui_inputs_for_sequence.append(click)


def create_hotkey(sequence_xml, hotkey_xml, ui_inputs_for_sequence):
    ui_input_index = list(sequence_xml).index(hotkey_xml)
    key = SendHotKey(ui_input_index)
    add_key_attributes(key, hotkey_xml)
    key_target = hotkey_xml.find('.//ui:SendHotkey.Target/ui:Target', ns)
    selector_service.add_selectors(key_target, key)

    # there should only be one target per click
    """
    for key_target in hotkey_xml.findall('.//ui:SendHotkey.Target/ui:Target', ns):
        selector_service.add_selectors(key_target, key)
    """

    ui_inputs_for_sequence.append(key)


def create_type_into(sequence_xml, type_into_xml, ui_inputs_for_sequence):
    ui_input_index = list(sequence_xml).index(type_into_xml)
    type_into = TypeInto(ui_input_index)
    add_type_into_attributes(type_into, type_into_xml)
    type_into_target = type_into_xml.find('.//ui:TypeInto.Target/ui:Target', ns);
    selector_service.add_selectors(type_into_target, type_into)

    # there should only be one target per click
    """
    for target in type_into_xml.findall('.//ui:TypeInto.Target/ui:Target', ns):
        selector_service.add_selectors(target, type_into)
    """

    ui_inputs_for_sequence.append(type_into)


def add_click_attributes(click: Click, click_element):
    if 'ClickType' in click_element.attrib:
        click.clickType = click_element.attrib['ClickType']
    if 'DisplayName' in click_element.attrib:
        click.name = click_element.attrib['DisplayName']
    if 'MouseButton' in click_element.attrib:
        click.mouseButton = click_element.attrib['MouseButton']


def add_key_attributes(key: SendHotKey, key_element):
    if 'DisplayName' in key_element.attrib:
        key.name = key_element.attrib['DisplayName']
    if 'Key' in key_element.attrib:
        key.key = key_element.attrib['Key']
    if 'KeyModifiers' in key_element.attrib:
        key.key_modifiers = key_element.attrib['KeyModifiers']


def add_type_into_attributes(type_into: TypeInto, key_element):
    if 'DisplayName' in key_element.attrib:
        type_into.name = key_element.attrib['DisplayName']
    if 'Text' in key_element.attrib:
        type_into.text = key_element.attrib['Text']


def append_sorted_ui_inputs(ui_inputs_for_sequence, sequence):
    sorted_ui_inputs_for_sequence = sorted(ui_inputs_for_sequence, key=lambda x: x.input_index, reverse=False)
    sequence.ui_input_array.extend(sorted_ui_inputs_for_sequence)
