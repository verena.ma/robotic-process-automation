*** Settings ***
Library     RPA.Cloud.Google
Suite Setup     Init Sheets     new-sheet-user.json

*** Variables ***
${SHEET_ID}=    ss
${SHEET_RANGE}=     Tabellenblatt1!A1:B2
${SHEET_RANGE_ADD}=     A:B

# +
*** Tasks ***
Read from sheet
    ${sheet_content}=   Get Sheet Values  ${SHEET_ID}  ${SHEET_RANGE}
    Log Many    ${sheet_content["values"]}
    
Add values to sheet
    ${new_values}=  Evaluate  [["Hello","new"]]
    # ROWS - default would be below each other
    Insert Sheet Values  ${SHEET_ID}  ${SHEET_RANGE_ADD}  ${new_values}  ROWS 

