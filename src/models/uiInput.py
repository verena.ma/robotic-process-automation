from src.models.selector import Selector

class UiInput:
    def __init__(self, input_index):
        self.input_index = input_index
        self.selector_tags = None
        self.selector: Selector | None = None
        self.name = None
