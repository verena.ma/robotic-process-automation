# -*- coding: utf-8 -*-
*** Settings ***
Library     RPA.Windows
Library     RPA.Browser
Library     RPA.Desktop

*** Tasks ***
Looking for elements
    #Control Window   type:PaneControl and name:'File Explorer'
# ${elements} is a list of `WindowsElement`s
#${elements}= Get Elements type:Window and name:'Number pad' > type:Button
    #${elements}=     Get Elements    type:ToolBarControl and name:'Ausgeführte Anwendungen' > name:'Google Chrome – 1 aktives Fenster'
    #type:ButtonControl and id:Chrome 
    
    Control Window    name:'Unbenanntes Dokument - Google Docs - Chromium'

    ${elements}=    Get Elements   type:Document and name:'Unbenanntes Dokument - Google Docs' > type:Image

    FOR  ${el}  IN  @{elements}
        #Log To Console   -----------------------
        Log    ${el}
        Log    ${el.name}                # access `WindowsElement`
        Log    ${el.item.AutomationId}   # access `WindowsElement.item` directly
        Log    ${el.item.Name}           # same as `${row.name}`
    END

*** Tasks ***
Minimal task
    #[Setup]  Windows Run   calc.exe
    #Control Window    executable:notepad++.exe
    Control Window    name:'*new 1 - Notepad++'
    Click   type:TabControl and name:'Tab' > name:'27 Jan. um 18_23.xaml'
    #Click  name:'*new 1 - Notepad++' > type:TabControl and name:'Tab' > name:'27 Jan. um 18_23.xaml'


# +
*** Tasks ***
Open Chrome and new Tab
    #[Setup]  Windows Run   calc.exe
    #Click   type:TabControl and name:'Tab' > name:'27 Jan. um 18_23.xaml'
    #Click  name:'*new 1 - Notepad++' > type:TabControl and name:'Tab' > name:'27 Jan. um 18_23.xaml'
    #Click   name:'Google Chrome – 2 aktive Fenster'
    
    Control Window   type:PaneControl and name:'Neuer Tab - Google Chrome'
    Click   type:ButtonControl and name:'Neuer Tab'
    # oder so
    #Click   type:PaneControl and name:'Neuer Tab - Google Chrome' > type:ButtonControl and name:'Neuer Tab'
    

# -

*** Tasks ***
open browser
    Open Browser  https://www.orf.at    chrome  #alias:BrowserA
    
    Click Element   tag:button

***Tasks***
test
    Windows Run   explorer.exe
    #Type Text       mail.google.com
    #RPA.Desktop.Press keys      enter
