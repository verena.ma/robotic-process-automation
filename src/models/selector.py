class Selector:
    def __init__(self):
        self.app = None
        self.app_type = None
        self.app_title = None
        self.app_cls = None
        self.uia_and_webctrl_array = None

    def __repr__(self):
        return "<Selector app:%s app_type:%s app_title:%s app_cls:%s uia_and_webctrl_array:%s>" % (
            self.app, self.app_type, self.app_title, self.app_cls, self.uia_and_webctrl_array)


class Uia_and_webctrl:
    def __init__(self):
        self.cls = None
        self.name = None
        self.role = None
        self.isleaf = None
        self.xml_tag = None
        self.tableRow = None
        self.tag = None
        self.aaname = None
        self.parent_id = None
        self.id = None
        # tbc

    def __repr__(self):
        return "<Uia_and_webctrl cls:%s name:%s role:%s isleaf:%s xml_tag:%s tableRow:%s tag:%s aaname:%s parent_id:%s id:%s>" % (
            self.cls, self.name, self.role, self.isleaf, self.xml_tag, self.tableRow, self.tag, self.aaname,
            self.parent_id, self.id)
