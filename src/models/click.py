from src.models.uiInput import UiInput


class Click(UiInput):
    def __init__(self, input_index):
        super().__init__(input_index)
        self.clickType = None
        self.mouseButton = None


    def __repr__(self):
        return "<Click clickType:%s name:%s mouseButton:%s selector:%s>" % (
            self.clickType, self.name, self.mouseButton, self.selector)
