***Settings***
Library     RPA.Browser
Library     RPA.Desktop

# +
*** Tasks ***
open browser
    Open Browser  https://covid19-dashboard.ages.at/    chrome  #alias:BrowserA
    #data needs to load
    Sleep  3s 
    ${element}=    Get Text  css:#tblLaender tbody tr:last-child .dispRel
    Get Element Count    css:#tblLaender tbody tr:last-child .dispRel    
    #${element}=     Get Element Count    xpath://table[@id="tblLaender"]//tbody//tr[last()]//td[@class="dispRel"]
    
    Log  ${element}
    
Log in
    Go To   https://mail.google.com/
    Click Element   xpath://input[@id="identifierId"]
    Type text  ***@gmail.com
    Click Element   xpath://div[@id="identifierNext"]//button
    Sleep   1s
    Click Element   xpath://div[@id="password"]
    Type text  ***
    Click Element   xpath://div[@id="passwordNext"]//button
    Sleep   5s
    Click Element   xpath://input[@id="idvPin"]
    #xpath://div[@id="view_container"]//section//ul//li[last()]
    Sleep  30s
    
prepare mail data  
    Click Element   xpath://div[@class="T-I T-I-KE L3"]
    
    Click Element   xpath://textarea[@name='to']
                    # xpath://textarea[@id=":at"]
    ${receiver}     Set Variable    ***@gmail.com
    Type text       ${receiver}
    #Click Element   xpath://div[@id=":en"]
    RPA.Desktop.Press Keys  enter
    #Sleep   1s
    
    Click Element   xpath://*[@name='subjectbox']
                    # xpath://input[@id=":ab"]
    ${subject}      Set Variable    Test subject
    Type text       ${subject}
 
write mail data  
    Click Element   xpath://div[@aria-label='Message Body']
                    #xpath://div[@id=":bh"]
    ${mail_text}    Set Variable    hello this is some mail text! :)
    Type text       ${mail_text}
    
Send mail
    Click Element   xpath://*[contains(@aria-label, '(Ctrl-Enter')]
                    #xpath://div[@id=":a1"]

    Sleep   5s
    Close Browser
# -
***Tasks***
test
    #Open Browser    https://mail.google.com    Chrome    alias=BrowserA
    #Open Chrome Browser     https://mail.google.com/
    #Open Available Browser     https://mail.google.com/     user_agent=Chrome
    Open Available Browser    https://mail.google.com    alias=BrowserA
    Sleep  2s
    #Close Browser

***Tasks***
test browser follw up
    Go To    http://www.orf.at
    #Switch Window    locator=NEW    browser=BrowserA
