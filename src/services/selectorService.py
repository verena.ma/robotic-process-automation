import xml.etree.ElementTree as ET
from src.models.click import Click
from src.models.sendHotKey import SendHotKey
from src.models.selector import Selector
from src.models.selector import Uia_and_webctrl
from src.models.typeInto import TypeInto


def add_selectors(target, element: Click | SendHotKey | TypeInto):
    selectors_string = extract_selectors_string(target)
    element.selector_tags = selectors_string
    selectors_xml = extract_selectors_xml(selectors_string)
    selector = Selector()

    # for uia in selectors_xml.findall('.//uia'):
    for i, selector_ in enumerate(selectors_xml):
        # first element contains information about selector type
        if i == 0:
            add_selector_attributes(selector_, selector)
        else:
            uiaAndWebctrlElement = create_uia_webctrl_element(selector_)
            selector.uia_and_webctrl_array.append(uiaAndWebctrlElement)

    if selector is not None:
        element.selector = selector


def extract_selectors_string(click_targ):
    selectors = click_targ.attrib['Selector']
    selectors = '<root>' + selectors + '</root>'
    # print('Selector:')
    # print(selectors)
    return selectors


def extract_selectors_xml(sel_string):
    # selectors_xml = ET.fromstring(selector_)
    # first_child = selectors_xml.findall('.//*[1]')[0]  # 1st child layer & first element
    selector_tree = ET.ElementTree(ET.fromstring(sel_string))
    selectors = list(selector_tree.getroot())
    return selectors


def add_selector_attributes(element, selector):
    app_type = element.tag
    selector.app_type = app_type

    selector.uia_and_webctrl_array = []
    if 'app' in element.attrib:
        app = element.attrib['app']
        selector.app = app
    if 'title' in element.attrib:
        app_title = element.attrib['title']
        selector.app_title = app_title
    if 'cls' in element.attrib:
        app_cls = element.attrib['cls']
        selector.app_cls = app_cls
    # click.selector = click_selector
    # return selector


def create_uia_webctrl_element(selector):
    ui_webctrl = Uia_and_webctrl()
    ui_webctrl.tag = selector.tag
    if 'name' in selector.attrib:
        ui_webctrl.name = selector.attrib['name']
    if 'cls' in selector.attrib:
        ui_webctrl.cls = selector.attrib['cls']
    if 'role' in selector.attrib:
        ui_webctrl.role = selector.attrib['role']
    if 'isleaf' in selector.attrib:
        ui_webctrl.isleaf = selector.attrib['isleaf']
    if 'tableRow' in selector.attrib:
        ui_webctrl.tableRow = selector.attrib['tableRow']
    if 'tag' in selector.attrib:
        ui_webctrl.tag = selector.attrib['tag']
    if 'aaname' in selector.attrib:
        ui_webctrl.aaname = selector.attrib['aaname']
    if 'parentid' in selector.attrib:
        ui_webctrl.parent_id = selector.attrib['parentid']
    if 'id' in selector.attrib:
        ui_webctrl.id = selector.attrib['id']

    return ui_webctrl
