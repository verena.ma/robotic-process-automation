# -*- coding: utf-8 -*-
*** Settings ***
Library     RPA.Desktop
Library     RPA.Windows
Library     RPA.Browser


# +
***Tasks***

open file explorer
#WND
#app     explorer.exe
#cls     Shell_TrayWnd
#-
#cls     MSTaskListWClass
#name    Ausgeführte Anwendungen
#-
#name    File Explorer
#role    button

    #Control Window    name:'Ausgeführte Anwendungen'
    RPA.Windows.Click   type:Button and name:'File Explorer'

open folder
#WND
#app     explorer.exe
#cls     CabinetWClass
#title   File Explorer
#-
#cls     SysTreeView32
#name    Tree View
#-
#name    Documents (pinned)
#role    tree item
    #Control Window   type:PaneControl and name:'File Explorer' # tyoe needed?
    Control Window   name:'File Explorer'
    RPA.Windows.Click   type:TreeItemControl and name:'Documents (pinned)'
    
open file
#WND
#app     explorer.exe
#cls     CabinetWClass
#title   Documents
#-
#cls     UIItemsView
#name    Items View
#-
#cls     UIProperty
#name    Name
#tableCol    0
#tableRow    3 (actually it's 5 now)
    Control Window   name:'Documents'
    RPA.Windows.Double Click   automationid:5
    #RPA.Windows.Double Click   name:'Items View' > automationid:5 >  name:'Name' 

copy whole text
#WND
#app     notepad.exe
#cls     Notepad
#title   TEST.txt - Notepad
#-
#cls     Edit
#name    Text Editor
    Control Window   name:'TEST.txt - Notepad'
    RPA.Desktop.Press keys  ctrl  a
    RPA.Desktop.Press keys  ctrl  c
    
open chromium
#WND
#app     explorer.exe
#cls     Shell_TrayWnd
#-
#cls     MSTaskListWClass
#name    Ausgeführte Anwendungen
#-
#name    Chromium
#role    button
    Control Window   class:Shell_TrayWnd #name:'Taskleiste'
    RPA.Windows.Click    type:Button and name:'Chromium'
   
open gdrive from favorites
#WND
#app     chrome.exe
#cls     Chrome_WidgetWin_1
#title   New Tab - Chromium
#-
#name    Bookmarks
#role    tool bar
#-
#name    Drive
#role    Bookmark button
    Control Window  name:'New Tab - Chromium'
    RPA.Windows.Click    type:Button and name:'Drive'

go to folder in gdrive
#HTML
#app     chrome.exe
#title   Meine Ablage – Google Drive
#-
#webctrl
#aaname      MA_process
#parentid    :4
#tag         DIV
    #Control Window  name:'Meine Ablage – Google Drive' # ! not working because the name at html is only from tab, not from windows --> 'Chromium' at the end is missing
    #Control Window    executable:chrome.exe
    # attach for RPA.Browser library, use debugging port
    RPA.Browser.Attach Chrome Browser    9222 
    RPA.Browser.Double Click Element   xpath://*[@id=':4']//div[contains(text(),'MA_process')]
    
create new gdoc
#WND
#app     chrome.exe
#cls     Chrome_WidgetWin_1
#title   MA_process – Google Drive - Chromium
#-
#name    Neu
#role    menu item
#---
#html
#app     chrome.exe
#title   MA_process – Google Drive
#-
#webctrl
#parentid    :4x
#tag         DIV
    Control Window  name:'MA_process – Google Drive - Chromium'
    RPA.Windows.Click    type:MenuItem and name:'Neu'
    Sleep   1s

    #RPA.Browser.Attach Chrome Browser    9222 
    #RPA.Browser.Click Element   xpath://*[@id=':4x']//div
    # new selector: <webctrl aria-role='menuitem' id=':4z' tag='DIV' />
    # more detailed?? //div[@id=':4z'][contains(@role, 'menuitem')]
    #RPA.Browser.Click Element   xpath://div[@id=':4z']
    # this selector was not working correctly
    # use instead (AA):
    Control Window  name:'MA_process – Google Drive - Chromium'
    RPA.Windows.Click    type:MenuItem and name:'Google Docs'
    
manipulate gdoc file
#HTML
#app     chrome.exe
#title   Unbenanntes Dokument - Google Docs
#-
#webctrl
#isleaf      1
#tag         svg
    RPA.Browser.Attach Chrome Browser    9222
    Switch Window    Unbenanntes Dokument - Google Docs
    # xpath for svgs: //*[local-name() = 'svg']
    # xpath leaf / no child elements: [not(*)]
    #RPA.Browser.Click Element   xpath://*[local-name() = 'svg'][not(*)]
    # returns 3 elements (1st would be correct)
    # xpath first occurance: (selector)[1]
    RPA.Browser.Click Element   xpath:(//*[local-name() = 'svg'][not(*)])[1]
    # alternative:
    #<wnd app='chrome.exe' cls='Chrome_WidgetWin_1' title='Unbenanntes Dokument - Google Docs - Chromium' />
    #<ctrl name='Unbenanntes Dokument - Google Docs' role='document' />
    #<ctrl idx='9' role='graphic' />
    
#paste text into gdoc
    Control Window   name:'Unbenanntes Dokument - Google Docs - Chromium'
    RPA.Desktop.Press keys  ctrl  v
# -

***Tasks***
Attach to running Chrome Browser and execute Google search
    RPA.Browser.Attach Chrome Browser    9222
    RPA.Browser.Go To    https://www.google.com/?hl=en
    RPA.Browser.Input Text    name:q    Dinosaur
    RPA.Browser.Press Keys    name:q    ENTER


    #Switch Window   title:Meine Ablage – Google Drive
    #Click Element   xpath://*[@id=':4']//*[contains(text(),'MA_process')]


***Tasks***
test
    RPA.Browser.Attach Chrome Browser    9222
    Get Window Titles
    RPA.Browser.Switch Window    Unbenanntes Dokument - Google Docs
    RPA.Browser.Click Element   xpath:(//*[local-name() = 'svg'][not(*)])[1]
    #RPA.Browser.Switch Window   strategy:title    "Unbenanntes Dokument - Google Docs"
