from src.models.uiInput import UiInput


class TypeInto(UiInput):
    def __init__(self, input_index):
        super().__init__(input_index)
        self.text = ''

    def __repr__(self):
        return "<TypeInto name:%s text:%s  selector:%s>" % (
            self.name, self.text, self.selector)
