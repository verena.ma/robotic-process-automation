*** Variables ***
${GMAIL_ACCOUNT}        
${GMAIL_PASSWORD}       
${RECIPIENT_ADDRESS}    
${IMAGEDIR}
${BODY_IMG1}            ${IMAGEDIR}${/}approved.png
${BODY_IMG2}            ${IMAGEDIR}${/}invoice.png
${EMAIL_BODY}           <h1>Hello Robot!</h1>

***Settings***
Library     RPA.Email.ImapSmtp   smtp_server=smtp.gmail.com  smtp_port=587
Task Setup    Authorize SMTP    ${GMAIL_ACCOUNT}    ${GMAIL_PASSWORD}    smtp.gmail.com    587

*** Tasks ***
Send Hello Mail
    Send Message    sender=${GMAIL_ACCOUNT}    recipients=${RECIPIENT_ADDRESS}    subject=Message from RPA Robot    body=${EMAIL_BODY}    html=${TRUE}

***Settings***
Library           RequestsLibrary
Library           RPA.JSON

***Tasks***
Get Weather Data for Vienna
#https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/vienna?unitGroup=metric&key=UTW3RHGK3BT2V3QHMJQYE2S8V&contentType=json
    ${resp}=    GET    url=https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/vienna?unitGroup=metric&key=UTW3RHGK3BT2V3QHMJQYE2S8V&contentType=json
    # Log    ${resp.json()}
    ${tempmin}=    Get value from JSON      ${resp.json()}    $.days[0].tempmin
    ${tempmax}=    Get value from JSON      ${resp.json()}    $.days[0].tempmax
    ${desc}=       Get value from JSON      ${resp.json()}    $.days[0].description
    Log    Heute hat es ${tempmin} bis ${tempmax} Grad! Es ist: ${desc}.
    
    ${tmrw_tempmin}=    Get value from JSON      ${resp.json()}    $.days[1].tempmin
    ${tmrw_tempmax}=    Get value from JSON      ${resp.json()}    $.days[1].tempmax
    ${tmrw_desc}=       Get value from JSON      ${resp.json()}    $.days[1].description
    Log    Morgen hat es ${tmrw_tempmin} bis ${tmrw_tempmax} Grad! Es wird: ${tmrw_desc}.
    
    ${WEATHER_EMAIL_BODY}    Set Variable    <h1>Guten Tag</h1><p>Heute hat es ${tempmin} bis ${tempmax} Grad! Es ist: ${desc}</p><p>Morgen hat es ${tmrw_tempmin} bis ${tmrw_tempmax} Grad! Es wird: ${tmrw_desc}</p><p>Quelle: https://www.visualcrossing.com/weather-api</p>
    Log    ${WEATHER_EMAIL_BODY}
    Send Message    sender=${GMAIL_ACCOUNT}    recipients=${RECIPIENT_ADDRESS}    subject=Verena's Wetter SoftwareBot    body=${WEATHER_EMAIL_BODY}    html=${TRUE}
