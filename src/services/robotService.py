from src.models.selector import Uia_and_webctrl
from src.models.click import Click
from src.models.sendHotKey import SendHotKey
from src.models.typeInto import TypeInto


def write_robot_file(sequences):
    write_start_of_robot_file()

    for sequence in sequences:
        task_lines = [create_sequence_name(sequence)]

        for task in sequence.ui_input_array:
            create_script_lines_for_task(task, task_lines)

        with open('./Robot1/python-created/python-created.robot', 'a', encoding='utf-8') as file:
            write_into_file(file, task_lines)


def create_sequence_name(sequence):
    # since * at start of line is not allowed at RPA Framework
    return sequence.name.replace("*", "-")


def create_script_lines_for_task(task, lines_task):
    if type(task) is Click:
        click_line: str = ''

        if task.selector.app_type == 'wnd':
            click_line = create_wnd_click_line(task, lines_task)
        elif task.selector.app_type == 'html':
            click_line = create_html_click_line(task, lines_task)

        click_line = check_for_close_window_task(click_line)

        lines_task.append(click_line)

    elif type(task) is SendHotKey:
        key_line = create_key_line(task, lines_task)
        lines_task.append(key_line)

    elif type(task) is TypeInto:
        type_into_line = create_type_into_line(task, lines_task)
        lines_task.append(type_into_line)


def add_wnd_selector_to_click_line(c_selector, click_line, selector_array, i):
    # special case
    if c_selector.tableRow:
        click_line = click_line + 'automationid:' + c_selector.tableRow + ' > '

    role = define_selector_role(c_selector.role)
    # capital_role = c_selector.role.capitalize()
    if role and role != '':
        click_line = click_line + role

    if c_selector.name:
        if role and role != '':
            click_line = click_line + ' and '
        click_line = click_line + 'name:\'' + c_selector.name + '\''
    if i < (len(selector_array) - 1):
        click_line = click_line + ' > '

    return click_line


def check_for_close_window_task(click_line):
    if click_line == '    RPA.Windows.Click    type:Button and name:\'Close\'':
        return '    Close Current Window'
    return click_line


def add_html_selector_to_click_line(c_selector, click_line):
    if c_selector.parent_id:
        click_line = click_line + '//*[@id=\'' + c_selector.parent_id + '\']'

    if c_selector.tag == 'svg':
        # special xpath for svgs: (//*[local-name() = 'svg'])
        # special xpath for leaf nodes: [not(*)]
        click_line = click_line + '//*[local-name() = \'svg\']'
    elif c_selector.tag and c_selector.aaname is None:
        click_line = click_line + '//' + c_selector.tag

    if c_selector.id:
        click_line = click_line + '[@id=\'' + c_selector.id + '\']'

    if c_selector.tag is None and c_selector.aaname:
        click_line = click_line + '//*[contains(text(),\'' + c_selector.aaname + '\')]'

    if c_selector.tag and c_selector.aaname:
        click_line = click_line + '//' + c_selector.tag + '[contains(text(),\'' + c_selector.aaname + '\')]'

    if c_selector.isleaf:
        click_line = click_line + '[not(*)]'

    return click_line


def define_selector_role(c_selector_role):
    role = ''
    if c_selector_role:
        match c_selector_role:
            case 'button':
                role = "type:Button"
            case 'tree item':
                role = "type:TreeItem"
            case 'items item':
                role = "type:TreeItem"
            case 'push button':
                role = "type:Button"
            case 'menu item':
                role = "type:MenuItem"
            case 'grouping':
                role = "type:Group"
            case 'document':
                role = "type:Document"
            case 'graphic':
                role = "type:Image"
            case 'text':
                role = ''  # windows group control 'TextControl' should work, but doesn't
            case 'banner':
                role = ''
            case _:
                role = ''

    return role


def create_wnd_click_line(task, lines_task):
    if task.selector.app_title:
        lines_task.append('    Control Window    name:\'' + task.selector.app_title + '\'')
    elif task.selector.app_cls:
        lines_task.append('     Control Window     class:\'' + task.selector.app_cls + '\'')

    if task.clickType == 'CLICK_SINGLE':
        click_line = '    RPA.Windows.Click    '
    else:
        click_line = '    RPA.Windows.Double Click    '

    selector_array: [Uia_and_webctrl] = task.selector.uia_and_webctrl_array
    for i, c_selector in enumerate(selector_array):
        click_line = add_wnd_selector_to_click_line(c_selector, click_line, selector_array, i)

    return click_line


def create_html_click_line(task, lines_task):
    # RPA.Browser.Attach Chrome Browser    9222
    # RPA.Browser.Double Click Element    xpath://*[@id=':4']//div[contains(text(),'MA_process')]
    # webctrl aaname='MA_process' parentid=':4' tag='DIV'
    lines_task.append('    RPA.Browser.Attach Chrome Browser    9222')
    lines_task.append('    RPA.Browser.Switch Window    locator=' + task.selector.app_title)

    click_line: str
    if task.clickType == 'CLICK_SINGLE':
        click_line = '    RPA.Browser.Click Element    xpath:'
    else:
        click_line = '    RPA.Browser.Double Click Element    xpath:'

    selector_array: [Uia_and_webctrl] = task.selector.uia_and_webctrl_array
    for i, c_selector in enumerate(selector_array):
        click_line = add_html_selector_to_click_line(c_selector, click_line)

    return click_line


def create_key_line(task, lines_task):
    if task.selector.app_title:
        lines_task.append('    Control Window    name:\'' + task.selector.app_title + '\'')
    key_line = '    RPA.Desktop.Press keys    '
    if task.key_modifiers:
        key_line = key_line + task.key_modifiers + '  '
    key_line = key_line + task.key

    return key_line


def create_type_into_line(task, lines_task):
    if task.selector.app_title:
        lines_task.append('    Control Window    name:\'' + task.selector.app_title + '\'')
    type_into_line = '    RPA.Desktop.Type text    '
    text_ = task.text
    text_ = text_.replace(" ", "\\ \\")
    type_into_line = type_into_line + text_ + '    enter=true'

    return type_into_line


def write_into_file(file, lines_task):
    for line in lines_task:
        file.write(line)
        file.write('\n')
    file.write('\n')
    file.write('\n')
    file.close()


def write_start_of_robot_file():
    with open('./Robot1/python-created/python-created.robot', 'a', encoding='utf-8') as file:
        file.write('# -*- coding: utf-8 -*-\n')
        file.write('*** Settings ***\n')
        file.write('Library     RPA.Desktop\n')
        file.write('Library     RPA.Windows\n')
        file.write('Library     RPA.Browser\n')
        file.write('\n# +\n***Tasks***\n\n')
        file.close()

