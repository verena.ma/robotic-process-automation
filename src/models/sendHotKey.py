from src.models.uiInput import UiInput


class SendHotKey(UiInput):
    def __init__(self, input_index):
        super().__init__(input_index)
        self.key = None
        self.key_modifiers = None

    def __repr__(self):
        return "<SendHotKey name:%s key:%s keyModifiers:%s  selector:%s>" % (
            self.name, self.key, self.key_modifiers, self.selector)
