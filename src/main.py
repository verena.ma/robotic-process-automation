import services.xmlParser as xmlParser
import services.robotService as robotService

sequences = xmlParser.parse_sequences("./xmls/task-capture.xaml")

print(str(sequences))

robotService.write_robot_file(sequences)

""" for testing puposes only """
# for x in root.findall('.//Flowchart', ns):
#     # print(x.attrib['DisplayName'])
#     print(x.attrib)  # show all attributes


# for x in root.findall('.//Flowchart.StartNode', ns):
#     print(x.tag)

# for elem in root.iter():
#     print(elem.tag)

# bla = root.find("./Flowchart/FlowStep/FlowStep.Next/FlowStep[@x:Name='__ReferenceID0']", ns)
# no dictionary support for attributes :(
# print(bla.attrib['{http://schemas.microsoft.com/netfx/2010/xaml/activities/presentation}WorkflowViewState.IdRef'])

###############################################

# find all clicks
# print('CLICKS:')
# for clickTarget in root.findall('.//ui:Click.Target/ui:Target', ns):
#    print(clickTarget.attrib['Selector'])
# ET.dump(click)

# find all steps
# print('STEPS')
# for flowStep in root.findall('.//FlowStep', ns):
#     print(flowStep.attrib['{http://schemas.microsoft.com/winfx/2006/xaml}Name'])
#     ET.dump(flowStep)
