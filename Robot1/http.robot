***Settings***
Library           Collections
Library           String
Library           RequestsLibrary
Library           RPA.JSON

# +
*** Test Cases ***

Get Request
    ${resp}=    GET    https://www.boredapi.com/api/activity
    Status Should Be    OK    ${resp}
    #Should Be Equal As Strings    ${resp.json()}[activity]    GET

# +
***Tasks***
Get Request
    ${resp}=    GET    https://www.boredapi.com/api/activity    params=participants[]=3
    Log    ${resp.json()}[activity]
    Log    ${resp.json()}

    
Use Json Library
    ${resp}=    GET    https://www.boredapi.com/api/activity    params=participants[]=3
    #${json}=    Convert String to JSON    {"orders": [{"id": 1},{"id": 2}]}
    ${value1}=     Get value from JSON      ${resp.json()}    $.activity
    Log    ${value1}

# -

***Tasks***
Get Weather Data for Vienna
    ${resp}=    GET    url=https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/vienna?unitGroup=metric&key=UTW3RHGK3BT2V3QHMJQYE2S8V&contentType=json
    
    Status Should Be    OK    ${resp}
    Log    ${resp.json()}
    
    ${tempmin}=    Get value from JSON      ${resp.json()}    $.days[0].tempmin
    ${tempmax}=    Get value from JSON      ${resp.json()}    $.days[0].tempmax
    Log    Heute hat es ${tempmin} bis ${tempmax} Grad!
