*** Settings ***
Library     RPA.Windows
Library     RPA.Browser

# +
*** Tasks ***

open chromium
    Control Window    class:Shell_TrayWnd #name:'Taskleiste'
    RPA.Windows.Click    type:Button and name:'Chromium'
    
    
***Tasks***
Attach to running Chrome Browser and execute Google search
    RPA.Browser.Attach Chrome Browser    9222
    RPA.Browser.Go To    https://www.google.com/?hl=en
    RPA.Browser.Input Text    name:q    Dinosaur
    RPA.Browser.Press Keys    name:q    ENTER
